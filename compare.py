# -*- coding: utf-8 -*-
"""
Cкрипт для сравнения json-отчетов cuckoo-sandbox. Требует наличия Python 3, fuzzywuzzy, python-Levenshtein
Принимает три параметра в качестве аргументов:
arg1 - имя первого отчета
arg2 - имя второго отчета
arg3 - имя результируюшего файла
"""
import sys
import csv
import json
from fuzzywuzzy import fuzz

RATIO = 20              # процент совпадений в строках, необходимый для того, чтобы они были приняты
MAX_LENGTH = 100000     # максимальная длина анализируемой строки (строки большего размера игнорируются)


def add_to_queue(el1, el2, key, queue):
    """
    Добавляет подлежащие сравнению элементы в очередь
    :param el1: 1-й элемент
    :param el2: 2-й элемент
    :param key: ключ
    :param queue: очередь
    :return:
    """
    if el1 is not None and el2 is not None:
        pair = (el1, el2, key)
        queue.append(pair)


def compare(file1, file2, ratio):
    """
    Производит сравнение отчетов
    :param file1: первый файл отчета
    :param file2: второй файл отчета
    :return: Тройка списков - ключ, строка из 1-го отчета, строка из второго отчета
    """

    data1 = json.load(file1)
    print("File loaded: " + str(file1.name))

    data2 = json.load(file2)
    print("File loaded: " + str(file2.name))

    keys, values1, values2, queue = [], [], [], []

    for key in data1.keys():
        el1 = data1.get(key)
        el2 = data2.get(key)
        add_to_queue(el1, el2, key, queue)

    sample_num = 0

    while len(queue) > 0:
        sample_num += 1
        pair = queue.pop()
        print("Comparing sample: " + str(sample_num))

        if not isinstance(pair[0], dict):
            str1 = str(pair[0])
            str2 = str(pair[1])
            if len(str1) < MAX_LENGTH and len(str2) < MAX_LENGTH \
                    and fuzz.ratio(str1, str2) > ratio \
                    and not (str1 == "[]" and str2 == "[]"):
                values1.append(str1)
                values2.append(str2)
                keys.append(pair[2])
            continue

        for key in pair[0].keys():
            el1 = pair[0].get(key)
            el2 = pair[1].get(key)
            add_to_queue(el1, el2, key, queue)

    print("Overall matches: " + str(len(keys)))
    return keys, values1, values2


def main(argv = None):
    """
    :param argv: Аргументы командной строки
    :return:
    """
    if argv is None:
        argv = sys.argv[1:]

    try:
        file1 = open(argv[0], 'r')
        file2 = open(argv[1], 'r')

        result = compare(file1, file2, RATIO)

        with open(argv[2] + '.csv', 'w') as f:
            w = csv.writer(f)
            w.writerows(result)

    except OSError as err:
        print("I/O error({0}): {1} : {2}".format(err.errno, err.strerror, err.filename))
    except IndexError as err:
        print("Not sufficient argument number")

main()